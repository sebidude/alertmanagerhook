package config

type AlertmanagerHookConfig struct {
	ListenAddress string                `json:"listenAddress"`
	LogFile       string                `json:"logFile"`
	Hooks         map[string]HookConfig `json:"hooks"`
}

type HookConfig struct {
	Type          string `json:"type"`
	URL           string `json:"url"`
	Channel       string `json:"channel"`
	ChannelField  string `json:"channelField"`
	User          string `json:"user"`
	UsernameField string `json:"usernameField`
	TextField     string `json:"textField"`
}
