package poster

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/sebidude/alertmanagerhook/pkg/alert"
	"gitlab.com/sebidude/alertmanagerhook/pkg/config"
)

type Poster struct {
	Config *config.AlertmanagerHookConfig
}

func (p *Poster) PostAlert(hook string, alertMsg alert.AlertMessage) error {
	hookconfig := p.Config.Hooks[hook]
	switch hookconfig.Type {
	case "mattermost":
		p.postMattermostMessage(hookconfig, alertMsg)
		return nil
	default:
		log.Printf("No such hook type: %s", hookconfig.Type)
		return fmt.Errorf("No such hook type: %s", hookconfig.Type)
	}

}

func (p *Poster) postMattermostMessage(hookconfig config.HookConfig, alertMsg alert.AlertMessage) error {
	chatClient := &http.Client{}

	var msgPayload string
	var statusIcon string
	for _, a := range alertMsg.Alerts {
		if a.Status == "firing" {
			statusIcon = ":sos:"
		}
		if a.Status == "resolved" {
			statusIcon = ":white_check_mark:"
		}

		msgPayload = fmt.Sprintf("%s* %s **%s**  \n  %s  \n  [source](%s)  \n",
			msgPayload,
			statusIcon,
			a.Annotation.Summary,
			a.Annotation.Description,
			a.GeneratorURL)
	}

	payload := fmt.Sprintf("{\"%s\":\"%s\",\"%s\":\"%s\",\"%s\":\"%s\"}",
		hookconfig.UsernameField,
		hookconfig.User,
		hookconfig.ChannelField,
		hookconfig.Channel,
		hookconfig.TextField,
		msgPayload)
	form := url.Values{}
	form.Add("payload", payload)
	req, _ := http.NewRequest("POST", hookconfig.URL, strings.NewReader(form.Encode()))

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(form.Encode())))
	response, err := chatClient.Do(req)
	if err != nil {
		return fmt.Errorf("Cannot post to channel: %s", err.Error())
	}
	if response.StatusCode%200 >= 100 {
		return fmt.Errorf("Posted with error: %s", err.Error())
	}
	return nil

}
