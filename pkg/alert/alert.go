package alert

import (
	"encoding/json"
)

type AlertAnotation struct {
	Summary     string `json:"summary"`
	Description string `json:"description"`
}

type Alert struct {
	Status       string                 `json:"status"`
	Labels       map[string]interface{} `json:"labels"`
	Annotation   AlertAnotation         `json:"annotations"`
	StartsAt     string                 `json:"startsAt"`
	EndsAt       string                 `json:"endsAt"`
	GeneratorURL string                 `json:"generatorURL"`
}

type AlertMessage struct {
	Receiver         string                 `json:"receiver"`
	Status           string                 `json:"status"`
	Alerts           []Alert                `json:"alerts"`
	GroupLabels      map[string]interface{} `json:"groupLabels"`
	CommonLabels     map[string]interface{} `json:"commonLabels"`
	CommonAnnotation AlertAnotation         `json:"commonAnnotations"`
	ExternalURL      string                 `json:"externalURL"`
	Version          string                 `json:"version"`
	GroupKey         json.Number            `json:"groupKey"`
}
