BASEDIR := $(shell pwd)
APPNAME := alertmanagerhook
APPSRC := cmd/$(APPNAME)/main.go

DESCRIPTION := Webhook receiver for alertmanager.

MAINTAINER := $(shell git log --max-count=1 --pretty="format:%an <%ae>" HEAD)

GITCOMMITHASH := $(shell git log --max-count=1 --pretty="format:%h" HEAD)
GITCOMMIT := -X main.gitcommit=$(GITCOMMITHASH)

VERSIONTAG := $(shell cat release.config)
VERSION := -X main.appversion=$(VERSIONTAG)

BUILDTIMEVALUE := $(shell date +%Y-%m-%dT%H:%M:%S%z)
BUILDTIME := -X main.buildtime=$(BUILDTIMEVALUE)

LDFLAGS := '-extldflags "-static" -d -s -w $(GITCOMMIT) $(VERSION) $(BUILDTIME)'
LDFLAGS_WINDOWS := '-extldflags "-static" -s -w $(GITCOMMIT) $(VERSION) $(BUILDTIME)'

clean:
	@rm -rf build

info: 
	@echo - appname:     "$(APPNAME)"
	@echo - verison:     "$(VERSIONTAG)"
	@echo - commit:      "$(GITCOMMITHASH)"
	@echo - buildtime:   "$(BUILDTIMEVALUE)" 
	@echo - maintainer:  "$(MAINTAINER)"
	@echo - description: "$(DESCRIPTION)"
dep:
	@go get -v -d ./...

linux: clean info dep
	@echo Building for linux
	@mkdir -p build/linux
	@CGO_ENABLED=0 \
	GOOS=linux \
	go build -o build/linux/$(APPNAME) -a -ldflags $(LDFLAGS) $(APPSRC)

run:
	@echo running $(APPNAME)
	go run $(APPSRC) -c example/config-test.yaml

rpm: linux
	@echo Create rpm
	cd $(BASEDIR)/build/linux && \
	fpm \
    	--prefix "/usr/bin" \
    	-t "rpm" \
    	-s "dir" \
		-n "$(APPNAME)" \
    	-a "x86_64" \
    	-m "$(MAINTAINER)" \
    	--description "$(DESCRIPTION) ($(GITCOMMITHASH))" \
    	--version "$(VERSIONTAG)" \
    	--iteration "1" \
    	.
