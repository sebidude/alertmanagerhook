package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"gitlab.com/sebidude/alertmanagerhook/pkg/alert"

	"github.com/alecthomas/kingpin"
	"github.com/gin-gonic/gin"
	"gitlab.com/sebidude/alertmanagerhook/pkg/config"
	"gitlab.com/sebidude/alertmanagerhook/pkg/logging"
	"gitlab.com/sebidude/alertmanagerhook/pkg/poster"
	"gitlab.com/sebidude/configparser"
)

var (
	buildtime     string
	gitcommit     string
	appversion    string
	listenaddress string
	configfile    string
	appConfig     *config.AlertmanagerHookConfig
	wg            sync.WaitGroup
)

type AlertPoster struct {
	Poster *poster.Poster
	Config *config.AlertmanagerHookConfig
}

func main() {
	kingpin.UsageTemplate(kingpin.CompactUsageTemplate)
	kingpin.Flag("config", "Full path to the configfile to be used.").Short('c').StringVar(&configfile)
	kingpin.Parse()

	log.SetFlags(log.Lshortfile | log.Ldate | log.Ltime | log.Lmicroseconds)

	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	router.Use(gin.Recovery())
	router.Use(logging.GinLogger())

	if len(configfile) > 1 {
		err := configparser.ParseYaml(configfile, &appConfig)
		if err != nil {
			log.Fatalf("Error reading configfile: %s", err.Error())
		}
	}

	alertposter := new(AlertPoster)
	alertposter.Config = appConfig
	alertposter.Poster = new(poster.Poster)
	alertposter.Poster.Config = appConfig

	router.POST("/alert/:hook", alertposter.ProcessAlert)

	log.Println("=====  AlertmanagerHook  =====")
	log.Printf("Builddate: %s", buildtime)
	log.Printf("Version  : %s", appversion)
	log.Printf("git      : %s", gitcommit)
	log.Println(" ---")
	log.Printf("Configfile : %s", configfile)

	httpsrv := &http.Server{
		Addr:    appConfig.ListenAddress,
		Handler: router,
	}
	wg.Add(1)
	go func() {
		defer wg.Done()
		log.Print(httpsrv.ListenAndServe())
	}()
	log.Println("HTTP server started.")

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	<-signalChan
	log.Println("Shutdown signal received, exiting.")
	httpsrv.Shutdown(context.Background())
	wg.Wait()
	log.Println("Bye.")
	os.Exit(0)
}

func (p *AlertPoster) ProcessAlert(c *gin.Context) {
	hookname := c.Param("hook")
	if _, ok := p.Config.Hooks[hookname]; !ok {
		log.Printf("No config found for hook '%s'", hookname)
		c.String(http.StatusNotFound, "No config found for hook '%s'", hookname)
		return
	}

	var alertMsg alert.AlertMessage
	err := c.ShouldBindJSON(&alertMsg)
	if err != nil {
		log.Printf("Cannot parse body to alert message '%s'", err.Error())
		c.String(http.StatusInternalServerError, "Cannot parse body to alert message '%s'", err.Error())
		return
	}
	err = p.Poster.PostAlert(hookname, alertMsg)
	if err != nil {
		log.Printf("Posting failed: '%s'", err.Error())
		c.String(http.StatusInternalServerError, "Posting failed: '%s'", err.Error())
		return
	}

	log.Printf("Posted alerts to %s", hookname)
	c.String(http.StatusOK, "Posted alerts to %s", hookname)

}
